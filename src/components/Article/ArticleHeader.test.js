import React from 'react';
import { mount } from 'enzyme';
import ArticleHeader from './ArticleHeader';

describe('<ArticleHeader />', () => {
    it('should render the article header', () => {
        const wrapper = mount(<ArticleHeader>An article header</ArticleHeader>);
        expect(wrapper.text()).toEqual('An article header');
    });

    it('should display "Untitled article" if no title is provided', () => {
        const wrapper = mount(<ArticleHeader/>);
        expect(wrapper.text()).toEqual('Untitled article');
    });
});