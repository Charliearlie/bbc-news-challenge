import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ArticleHeader from './ArticleHeader';
import ArticleMain from './ArticleMain';

const ArticleWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 4fr 1fr;
  margin: 4px 0 16px 0;
`;

const ArticleContent = styled.article`
  grid-column: 2;
`;

const Article = ({ article }) => {
  return ( 
    <ArticleWrapper>
      <ArticleContent>
        <ArticleHeader>{article.title}</ArticleHeader>
        <ArticleMain articleBody={article.body} />
      </ArticleContent>
    </ArticleWrapper>
  );
}

Article.propTypes = {
  article: PropTypes.shape({
    title: PropTypes.string.isRequired,
    body: PropTypes.array.isRequired,
  }).isRequired,
};
 
export default Article;
