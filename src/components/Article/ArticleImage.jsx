import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ImageWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 16px;
  flex-direction: column;
  align-items: center;
`;

const Figure = styled.figure`
  font-style: italic;
  font-size: 0.9rem;
`;

const ArticleImage = ({ model }) => {
  const { url, altText, height, width } = model;
  return (  
    <ImageWrapper>
      <img src={url} alt={altText} height={height} width={width} />
      {altText && <Figure>{altText}</Figure>}
    </ImageWrapper>
  );
}

ArticleImage.propTypes = {
  model: PropTypes.shape({
    url: PropTypes.string.isRequired,
    altText: PropTypes.string,
    height: PropTypes.string,
    width: PropTypes.string,
  }),
};
 
export default ArticleImage;
