import React from 'react';
import { mount } from 'enzyme';
import Article from './Article';
import ArticleHeader from './ArticleHeader';
import ArticleParagraph from './ArticleParagraph';
import ArticleImage from './ArticleImage';
import ArticleList from './ArticleList';

const testArticle = {
  title: "A test article title",
  body: [
    {
      type: "heading",
      model: {
        text: "A test article title"
      }
    },
    {
      type: "paragraph",
      model: {
        text: "A test article paragraph"
      }
    },
    {
      type: "image",
      model: {
        url: "https://testarticlephoto.jpg",
        altText: "A test article photo",
        height: "420",
        width: "640"
      }
    },
    {
      type: "list",
      model: {
        type: "unordered",
        items: [
          "JavaScript",
          "React",
          "Redux",
          "CSS/SASS",
          "Node.js",
          "MongoDB",
          "Unit testing (Mocha/Enzyme/Chai/React Testing Library)",
          "UX",
          "Java"
        ]
      },
    }
  ]
};

const wrapper = mount(<Article article={testArticle}/>);

describe('<Article/>', () => {
  it('should render the article title', () => {
    const header = wrapper.find(ArticleHeader);
    expect(header.text()).toEqual('A test article title');
  });

  it('should render a paragraph', () => {
    const paragraph = wrapper.find(ArticleParagraph)
    expect(paragraph.length).toBe(1);
    expect(paragraph.text()).toEqual(testArticle.body.find(x => x.type === 'paragraph').model.text)
  });

  it('should render an image', () => {
    const image = wrapper.find(ArticleImage)
    expect(image.length).toBe(1);
    expect(image.props()).toStrictEqual({
      model: {
        url: "https://testarticlephoto.jpg",
        altText: "A test article photo",
        height: "420",
        width: "640"
      }
    })
  });

  it('should render a list', () => {
    const list = wrapper.find(ArticleList);
    expect(list.length).toBe(1);
  });
});