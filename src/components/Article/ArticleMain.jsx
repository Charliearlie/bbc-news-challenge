import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ArticleParagraph from './ArticleParagraph';
import ArticleImage from './ArticleImage';
import ArticleList from './ArticleList';

const articleMap = {
  paragraph: ArticleParagraph,
  image: ArticleImage,
  list: ArticleList,
};

const ArticleMainWrapper = styled.div`
  margin: 16px;
`;

const ArticleMain = ({ articleBody }) => {
  const articleContents = articleBody.filter(item => item.type !== 'heading');
  return (
    <ArticleMainWrapper>
      {articleContents.map((item, index) => {
        const Component = articleMap[item.type];
        return <Component key={`${item.type}_${index}`} model={item.model} />
      })}
    </ArticleMainWrapper>
  );
}

ArticleMain.propTypes = {
  articleBody: PropTypes.array.isRequired,
}
 
export default ArticleMain;