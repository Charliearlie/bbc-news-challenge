import React from 'react';
import styled from 'styled-components';

const ArticleList = ({ model }) => {
  const items = (items) => (
    items.map(item => (
      <li key={item}>{item}</li>
    ))
  );

  if (model.type === 'unordered') {
    return (
      <ul>
        {items(model.items)}
      </ul>
    )
  }
  return (
    <ol>
      {items(model.items)}
    </ol>
  );
}
 
export default ArticleList;