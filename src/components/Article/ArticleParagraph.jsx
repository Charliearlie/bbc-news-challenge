import React from 'react';
import PropTypes from 'prop-types';

const ArticleParagraph = ({ model }) => (
  <p>
    {model.text}
  </p>
);

ArticleParagraph.propTypes = {
  model: PropTypes.shape({
    text: PropTypes.string.isRequired,
  }),
};
 
export default ArticleParagraph;
