import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const HeaderWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
`;

const Header = styled.h1`
  font-weight: 800;
  font-size: 1.8rem;
  text-align: center;
`;

const ArticleHeader = ({ children }) => {
  return (
    <HeaderWrapper>
      <Header>{children}</Header>
    </HeaderWrapper>
  );
}

ArticleHeader.propTypes = {
  children: PropTypes.node,
};

ArticleHeader.defaultProps = {
  children: 'Untitled article',
};
 
export default ArticleHeader;