import React from 'react';
import styled from 'styled-components';

const ArticleCard = styled.div`
  display: flex;
  flex-direction: row;
  padding: 4px;
  background-color: #fff;
  box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
  margin: 8px;
`;

const ArticleTitleWrapper = styled.div`
  padding: 8px;
`;

const RankingArticleCard = ({ article, onClick, index }) => (
  <ArticleCard>
    <img src={`https://via.placeholder.com/150x150?text=Article+${index + 1}`} alt={`Article+${index}`} />
    <ArticleTitleWrapper>
      <h3>{article.title}</h3>
    </ArticleTitleWrapper>
  </ArticleCard>
);
 
export default RankingArticleCard;