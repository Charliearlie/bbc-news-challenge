import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import RankingArticleCard from './RankingArticleCard';

const RankingWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 64px;
  margin: 12.5% 25%;
`;

class Ranking extends Component {
  constructor(props) {
    super(props);
    this.state = {  }
  }

  render() { 
    const articles = this.props.articles.map((article, index) => ({
      ranking: index + 1,
      ...article
    }))
    .sort((a, b) => a.ranking - b.ranking)
    return (
      <RankingWrapper>
        {articles.map((article, index) => (
          <RankingArticleCard draggable key={article.title} article={article} index={index} />
        ))}
      </RankingWrapper>
    );
  }
}

Ranking.propTypes = {
  articles: PropTypes.shape({
    title: PropTypes.string.isRequired,
    body: PropTypes.array.isRequired,
  }),
};
 
export default Ranking;