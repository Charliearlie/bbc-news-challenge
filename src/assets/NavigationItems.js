const left = [
    {
        title: "Read",
        link: "/",
    },
    {
        title: "Submit",
        link: "/submit",
    },
];

const right = [
    {
        title: "Github",
        link: "https://github.com/charliearlie",
    },
    {
        title: "LinkedIn",
        link: "www.linkedin.com/in/charlie-waite"
    },
];

export default { left, right };
