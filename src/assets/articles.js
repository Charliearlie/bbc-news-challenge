import articleOne from './article.json';
import articleTwo from './article2.json';
import articleThree from './article3.json';
import articleFour from './article4.json';
import articleFive from './article5.json';

const articles = [articleOne, articleTwo, articleThree, articleFour, articleFive];

export function getRandomArticle(withDelay = false) {
  // I deliberately made the random index possibly be out of range to deal with errors
  const index = Math.floor((Math.random() * articles.length));
  const article = articles.splice(index, 1)[0];

  return new Promise((resolve, reject) => {
    if (article) {
      setTimeout(() => {
        resolve(article)
      }, withDelay ? 1000 : 0);
    } else {
        reject({ message: 'article not found'})
    }
  });
}