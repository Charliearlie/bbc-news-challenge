import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import Article from './components/Article/Article';
import Navbar from './Navbar';
import NavigationItems from './assets/NavigationItems';

import { getRandomArticle } from './assets/articles';
import Ranking from './components/Ranking/Ranking';

const AppWrapper = styled.div`
  font-family: 'Open Sans', sans-serif;
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
`

const NextButton = styled.button`
  border-radius: 2px;
  color: white;
  background-color: #bb1919;
  border: 0;
  padding: 16px;
  cursor: pointer;
  &:hover {
    background-color: #ccc
  }
`

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      article: null,
      articlesRead: [],
      articlesLimit: 3,
      error: null,
      limitReached: false,
    };
  
    this.randomArticle = this.randomArticle.bind(this);
  }

  componentDidMount() {
    this.randomArticle();
  }

  randomArticle(withDelay) {
    if(this.state.articlesRead.length < this.state.articlesLimit) {
      getRandomArticle(withDelay)
        .then(article => {
          this.setState(prevState => {
            const articlesRead = prevState.articlesRead;
            articlesRead.push(article);
            
            return {
              articlesRead,
              article,
              error: null,
            }
          });
        })
        .catch(error => this.setState({ error: error.message, article: null }));
    } else {
      this.setState({ limitReached: true })
    }
  }

  showArticle() {
    const { article, error } = this.state;
    return (
      <Fragment>
        {article &&
          <Article article={article}/>
        }
        {error && 
          <h1>{error}</h1>
        }
        <ButtonContainer>
          <NextButton
            onClick={() => this.randomArticle(true)}
          >
            {error ? 'Try again' : 'Next random article'}
          </NextButton>
        </ButtonContainer>
      </Fragment>
    );
  }

  showRanking() {
    return (
      <Ranking articles={this.state.articlesRead} />
    );
  }

  render() {
    const { limitReached } = this.state;
    return (
      <AppWrapper>
        <Navbar leftItems={NavigationItems.left} rightItems={NavigationItems.right} />
        {!limitReached ?
          this.showArticle() : 
          this.showRanking()
        }
      </AppWrapper>
    );
  }
}

export default withRouter(App);
