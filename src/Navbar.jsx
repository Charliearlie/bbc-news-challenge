import React, { Component } from 'react';
import styled from 'styled-components';
import NavLink from './NavLink';

const NavbarWrapper = styled.div`
  background-color: #bb1919;
  display: flex;
  padding: 16px;
  font-family: sans-serif;
  color: white;
`;

const NavbarBrand = styled.div`
  font-weight: bold;
  margin-right: 20px;
`;

const NavbarItems = styled.ul`
  display: flex;
  list-style-type: none;
  margin: 0;
`;

const NavbarItemsRight = NavbarItems.extend`
  margin-left: auto;
`;

const NavbarItem = styled.li`
  padding: 0 8px;
`;

const Navbar = props => ( 
  <NavbarWrapper>
    <NavbarBrand>Random News</NavbarBrand>
    <NavbarItems>
      {props.leftItems.map(item => (
        <NavbarItem>
          <NavLink to={item.link}>{item.title}</NavLink>
        </NavbarItem>
      ))}
    </NavbarItems>
    <NavbarItemsRight>
      {props.rightItems.map(item => (
        <NavbarItem>
          <NavLink to={item.link}>{item.title}</NavLink>
        </NavbarItem>
      ))}
    </NavbarItemsRight>
  </NavbarWrapper>
);
 
export default Navbar;
