import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const NavLink = ({ to, children, style }) => (
  <Link style={style} to={to}>
    {children}
  </Link>
);

NavLink.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  style: PropTypes.object,
};

NavLink.defaultProps = {
  style: {
    color: 'white',
    textDecoration: 'none',
  },
};

export default NavLink;
